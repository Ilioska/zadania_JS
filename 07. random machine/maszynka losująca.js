window.onclick = start;
var array = [];
var lastAverage = [];
var averageChange = ['number decreases the mean','number increases the mean','number does not affect the mean'];

function start() {
    var number = Math.floor(Math.random()*100)+1;
    array.push(number);
    console.log(array);
    document.querySelector('.box').textContent = number;

    var drawNumber = array.length;
    console.log('nr losowania',drawNumber);
    document.querySelector('.NumerLosowania').textContent = drawNumber;

    var sum = 0;
    for (var i = 0; i < array.length; i++) {
        sum += array[i];
    }
    console.log('suma',sum);
    var average = sum / array.length;
    console.log('średnia',average);
    average < lastAverage[0] ? document.querySelector('.Wpływ').textContent = 'number decreases the mean' : null;
    average > lastAverage[0] ? document.querySelector('.Wpływ').textContent = 'number increases the mean' : null;
    average == lastAverage[0] ? document.querySelector('.Wpływ').textContent = 'number does not affect the mean' : null;
    lastAverage.splice(0, 1, average);
    document.querySelector('.Średnia').textContent = average.toFixed(2);

    var deviation = Math.abs(number - average);
    console.log('odchylenie',deviation);
    document.querySelector('.Odchylenie').textContent = deviation.toFixed(2);

    //częstość losowania number w %
    var numberArray = [];
    for (var i = 0; i < array.length; i++) {
        numberArray.push(array.indexOf(number, i));
        i = array.indexOf(number, i);
    }
    console.log(numberArray);
    var frequency = (numberArray.length / array.length) * 100;
    console.log('częstość',frequency);
    document.querySelector('.Częstość').textContent = frequency.toFixed(2);
}

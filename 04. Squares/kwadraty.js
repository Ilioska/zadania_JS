var array = [];
var squareNumber = Math.floor(Math.random()*3);

/* pętla do wylosowania 3 różnych cyfr i dodanie ich do tablicy */
for (var i = 0; i <= 2; i++) {
    var digit = Math.floor(Math.random()*10);
    array.indexOf(digit) === -1 ? array.push(digit) : i--; //.push() dodaje do tablicy na ostatnie miejsce
}

for (i = 0; i < array.length; i++) {
    var square = document.querySelector(".square" + (i+1));
    square.children[0].textContent = array[i]; //dodaje wylosowane cyfry do kwadratów
    i === squareNumber ? square.classList.add('squareactive') : null; //dodaje klasę aktywną do wylosowanego kwadratu
}

function setQuot() {
    document.querySelector('.rectanglebottom').textContent = document.querySelector('.squareactive').title; //pobiera title z obecnego aktywnego kwadratu i dodaje cytat do dolnego prostokąta
}

function changeActive(event) {
    var e = event.target;
    var parentClasses = e.parentNode.classList;

    if (e.classList.contains('squaretable') && !parentClasses.contains('squareactive')) {  //event.target pobiera element jak document.querySelector / .contains() sprawdza czy element zawiera daną klasę / wykrzyknik przed wyrażeniem zaprzecza
        document.querySelector('.squareactive').classList.remove('squareactive'); //.remove() usuwa element z drzewa DOM
        parentClasses.add('squareactive');
//        setQuot();
    }
}
setQuot();
document.ondblclick = changeActive;

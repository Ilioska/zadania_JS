window.onload = start;

var array = [
    ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','W','X','Y','Z'],
    ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','r','s','t','u','w','x','y','z'],
    [0,1,2,3,4,5,6,7,8,9],
    ['!','@','#','$','%','^','&','*','?']
]

function start() {
    var x = [0,0,0,0];
    var Y = [];
    for (var i = 0; i <= 7; i++) {
        var digit = Math.floor(Math.random()*4);

        i > 3 && x.indexOf(0) !== -1 ? digit = x.indexOf(0) : null;

        i == 0 || i == 7 ? digit = Math.floor(Math.random()*3) : null;

        Y.length > 2 ? Y.splice (0, 1) : null;

        Y.push(digit);

        if (Y[0] == Y[1] && Y[1] == Y[2]) {
            i--;
            console.log('Y');
            continue; //zatrzyma iterację i odpali ją od kolejnego i, nie wykona tego co jest poniżej
        }

        x[digit] == 0 ? x.splice(digit, 1, 1) : null;

        var Arr = array[digit];
        var token = Math.floor(Math.random()*(Arr.length));
        var box = document.querySelector('.box');

        if (box.textContent.indexOf(Arr[token]) !== -1) {
            console.log('tak');
            i--;
            continue;
        }

        box.textContent += Arr[token];
    }
}
